<!--t Cara Instal Artix Linux Menggunakan Live CD Base Iso t-->
<!--d Video tutorial Cara Instal Artix Linux Menggunakan Live CD Base Iso. Kemudian dilanjutkan dengan instal compiz sebagai desktop environment. d-->
<!--tag artix linux,tutorial,operating system,linux tag-->
<!--video hYAEEHJcZ88 video-->

<b>Artix Linux</b> adalah distro linux berbasis Arch Linux, atau lebih tepatnya derivatif atau turunan dari Arch Linux, yang berbasis rolling-release distribution. Beberapa tahun terakhir memang Arch lagi naik daun karena istilah "rolling-release" dan <a href="https://crackjack.info/post/portable-ccleaner-v5919537-professional" alt="System Optimizer">"bleeding edge"</a> yang artinya selalu terupdate kapan saja, tanpa kenal waktu, gak seperti distro mainstream lainnya. Hal lain yang bikin Arch terkenal adalah karena sulitnya proses penginstalan, terutama untuk para pemula, tapi entah kenapa hal ini malah jadi tantangan tersendiri buat yang mau belajar uji nyali. Sehingga muncul jargon "btw, I use Arch" yang artinya semacam olok-olokan jika seseorang itu "hebat" karena udah instal Arch. Saking terkenalnya, Arch Linux punya banyak turunan, salah satunya Artix Linux yang akan kita bahas ditutorial kali ini. Bedanya adalah karena Artix Linux "non systemd", menggunakan init system selain systemd. Dan tutorial ini akan membahas instalasi Artix Linux dimana openRC sebagai init system-nya. Dan karena tutorial ini akan panjang, maka kita bagi jadi dua part.

<img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgrT-FL5Wj-svcDrhp--u2K3UttOJmaxy3RDvb9GprOfIdHmue2g7nDqctRJ_VyM5Yq61KtSmaQNgwlL_cm9wUSSfFVqh89inAOMRcsi-H4XDpNtQrIG1Hrtz8PPToIgKOG20ZeoBTuL3n4-tyXXFplqAOpvFXTAZpwLQ6FLWvfCQHd95IhVfOCGsBVlA/s16000/artixcompiz2.jpg" alt="Artix Linux with Compiz Desktop" />

<h3>Daftar Isi</h3>
<div class="Spoiler">
    <div class="tombol" tabindex="0"></div>
    <div class="isi">
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#1">1. Download Live CD ISO</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#2">2. Disk Partitioning</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#2a">a. Format partition</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#2b">b. Mount partition</a></br></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#3">3. Install Base System</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#3a">a. Base and init script</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#3b">b. Instal kernel</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#3c">c. Fstabgen</a></br></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#4">4. Chrooting</a></br></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#5">5. Set Pacman and Mirrors</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#5a">a. Set pacman</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#5b">b. Set makepkg</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#5c">c. Update database</a></br></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#6">6. Set Waktu dan Bahasa</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#6a">a. Set waktu</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#6b">b. Set bahasa</a></br></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#7">7. Instal Bootloader</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#8">8. Tambah user/pengguna</a></br></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#9">9. Pengaturan Jaringan</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#9a">a. Create hostname files</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#9b">b. Install connman</a></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#9c">c. Konfigurasi connman</a></br></br>
<a href="https://crackjack.info/post/cara-instal-artix-linux-menggunakan-live-cd-base-iso#10">10. Reboot The System</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#11" target="_blank">11. Instal Yaourt</a></br></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#12" target="_blank">12. Instal Drivers</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#12a" target="_blank">a. Driver grafis</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#12b" target="_blank">b. Driver audio</a></br></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#13" target="_blank">13. Instal Display Manager</a></br></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#14" target="_blank" alt="Install Compiz Desktop">14. Instal Compiz Desktop</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#14a" target="_blank">a. Desktop Environment</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#14b" target="_blank">b. Create file compiz-session</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#14c" target="_blank">c. Create a desktop file for compiz session</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#14d" target="_blank">d. Create a .compiz-session file</a></br></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#15" target="_blank">15. Fungsi Dasar</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#16" target="_blank">16. Install Goodies</a></br></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#17" target="_blank">17. Misc</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#17a" target="_blank">a. Customize oblogout</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#17b" target="_blank" alt="Simple wallpaper changer with feh">b. Simple wallpaper changer with feh</a></br>
<a href="https://crackjack.info/post/cara-instal-compiz-desktop-di-artix-linux#17c" target="_blank">c. Configure Localepurge</a></br>
</div>
</div>

<h3 id="1">1. Download Live CD ISO</h3>
Untuk sekadar coba-coba, Anda bisa praktekkan instalasi diVirtualbox. Pertama-tama download dulu isonya di <a href="https://artixlinux.org/download.php" rel="nofollow" target="_blank">Artix Download Page</a> trus pilih iso artix-base-openrc, ukurannya sekitar ~700MB. Setelah download, Anda bisa burning CDnya, atau bikin live bootable USB pake unetbootin, rufus, atau apapun itu. Kalau mimin udah terbiasa pakai dd command.

Jadi pastikan dulu USB Anda sdb atau sdc
<code>sudo fdisk –l</code>
atau</br>
<code>lsblk</code>

Trus gunakan dd command, semisal USB Anda /dev/sdb atau /dev/sdc dan seterusnya
<code>sudo dd if=/pathtoyourisofile/artix.iso of=/dev/sdb</code>
if itu perintah input, letak iso Anda berada, of itu output Anda atau USB Anda.

<h3 id="2">2. Disk Partitioning</h3>
Silahkan boot ke instalasi live CD atau USB, trus login pake username yang disediakan, biasanya username: artix, password: artix. Pastikan Anda sudah terhubung ke internet, baik itu via LAN atau USB tethering. Karena proses instalasi ini akan banyak mengunduh paket dari internet.

<h4 id="2a">a. Format partition</h4>
Untuk format disk kita menggunakan cfdisk
<code>sudo cfdisk</code>

Biasanya instalasi dasar linux cuma butuh 5GB, tapi terserah Anda mau berapa ukurannya. Kalau mimin pribadi biasanya bikin 3 partisi, sda1 untuk EFI boot partisi, sda2 untuk sistem, dan sda3 untuk home. Untuk EFI atau sda1, 100MB cukup, sda2 atau system biasanya mimin kasih 15GB. Sisanya untuk home partition.

Setelah pembagian disk, saatnya quit cfdisk dan formatiing. Tiap disk kita kasih label, sda1 labelnya BOOT, sda2 ROOT, dan sda3 HOME untuk lebih mudah mengingat.
<code>sudo mkfs.fat -F 32 /dev/sda1
sudo fatlabel /dev/sda1 BOOT
sudo mkfs.ext4 -L ROOT /dev/sda2        <- root partition
sudo mkfs.ext4 -L HOME /dev/sda3        <- home partition, optional</code>
Home partition bisa gabung dengan sistem, jadi dibuatin ukurannya gede banget, makanya optional.

<h4 id="2b">b. Mount partition</h4>
Sebelum instal apapun, disk harus dimount dulu kan. Pertama mount dulu ROOT Partition
<code>sudo mount /dev/sda2 /mnt</code>

Habis itu bikin folder dalam /mnt untuk mounting point sda1 dan sda3
<code>sudo mkdir /mnt/boot
sudo mkdir /mnt/home</code>

Barulah mounting sda1 dan sda3 ke dalam folder atau mounting point tadi
<code>sudo mount /dev/sda1 /mnt/boot
sudo mount /dev/sda3 /mnt/home</code>

<h3 id="3">3. Install Base System</h3>
Base system atau dasar dari suatu sistem linux, biasanya berupa set tools, init script atau alat untuk compiling paket lain.

<h4 id="3a">a. Base and init script</h4>
<code>basestrap -i /mnt base base-devel openrc elogind-openrc</code>

<h4 id="3b">b. Instal kernel</h4>
Kernel adalah otak dari sistem Linux. Anda bisa memilih antara kernel biasa atau <i>linux-zen</i>. Instal juga <a href="https://olderdeserved.com/pt9s4dh3g?key=fdd5b94e99220fe83e176ef790d9d5cc" rel="nofollow" target="_blank">linux-firmware</a> untuk dukungan atau driver berbagai macam device. Setelah instal kernel, biasanya perintah "mkinitcpio -p linux" akan berjalan otomatis untuk meng-generate image file dari linux yang akan berjalan saat boot up. Biasanya ada tiga file: vmlinuz, initramfs, dan initramfs-fallback.
<code>basestrap /mnt linux linux-firmware linux-headers nano</code>

<h4 id="3c">c. Fstabgen</h4>
Ini seperti semacam list atau daftar partisi yang Anda miliki tercatat dalam fstab file. Biasanya berupa kode angka atau label dari partisi. Masuk dulu sebagai root.
<code>su root   <- login sebagai root
fstabgen -U /mnt >> /mnt/etc/fstab
exit   <- keluar dari root
</code>

<h3 id="4">4. Chrooting</h3>
Chrooting adalah proses masuk ke dalam sistem secara administratif dengan hak sebagai root. Biasanya tanpa password, chrooting berguna untuk mengoperasikan sejumlah perintah, dalam hal ini instalasi dan konfigurasi sistem.
<code>artix-chroot /mnt</code>

<h3 id="5">5. Set Pacman and Mirrors</h3>
Pacman itu package manager untuk distro Arch dan turunannya. Tugasnya untuk mengurus soal instalasi, dependensi. Pokoknya kalau berurusan sama instal software, pacman lah jawaranya.

<h4 id="5a">a. Set pacman</h4>
Untuk mengatur pacman, pastikan SigLevel set ke "Never" semua, untuk memudahkan proses instalasi. Kalau kata orang-orang itu gak boleh dilakukan, ngetes integritas paket adalah bagian dari keamanan. Tapi selama ini mimin instal paket tanpa cek hash aman-aman aja tuh, jadi silahkan ikut saran saya, gak usah <a href="https://nessainy.net/4/4938810" rel="nofollow" target="_blank">cek integrity.</a> Dan ingat untuk uncomment (hilangkan tanda pagar) pada repo yang diinginkan untuk enable. Kalau saran mimin sih enable aja semua, termasuk gremlins dan lib32-gremlins.
<code>nano /etc/pacman.conf</code>

Silahkan tambahkan beberapa repo penting dibawah ini. Universe dan Omniverse itu repo khusus Artix. Tapi semua turunan Arch bisa pakai kok.
<code>[universe]
SigLevel = Never
Server = https://universe.artixlinux.org/$arch
Server = https://mirror1.artixlinux.org/universe/$arch
Server = https://mirror.pascalpuffke.de/artix-universe/$arch
Server = https://artixlinux.qontinuum.space:4443/universe/os/$arch
Server = https://mirror.alphvino.com/artix-universe/$arch</br>
[omniverse]
SigLevel = Never
Server = http://omniverse.artixlinux.org/$arch</code>

Konfigurasi dasar base, dengan menambahkan support dari repo Arch
<code>pacman -Sy && pacman -S artix-archlinux-support</code>

Dibawah ini adalah repo Arch, tambahin aja, biar makin banyak software yang bisa Anda observasi.
<code>[extra]
Include = /etc/pacman.d/mirrorlist-arch</br>
[community]
Include = /etc/pacman.d/mirrorlist-arch</br>
[multilib]
Include = /etc/pacman.d/mirrorlist-arch</code>

<h4 id="5b">b. Set makepkg</h4>
Makepkg biasanya digunakan untuk compiling software yang gak ada direpo, biasanya ada diaur.
<code>nano /etc/makepkg.conf
MAKEFLAGS="-j5"   <- ubah sebanyak jumlah core +1</code>

Untuk jumlah core, misalnya Anda menggunakan cpu intel 8 core, jadi silahkan diset 9. Hal ini untuk mempercepat proses compiling. Ctrl+X untuk save dan exit dari nano.

<h4 id="5c">c. Update database</h4>
Masuk pakai nano lagi
<code>nano /etc/pacman.d/mirrorlist   <- pilih Asia saja
nano /etc/pacman.d/mirrorlist-arch   <- pilih Indo saja</code>

Untuk mirrorlist mimin sarankan pilih yang paling dekat dengan wilayah tempat tinggal Anda. Uncomment semua server Asia dan Indonesia. Lalu setelah exit nano, silahkan update database.
<code>pacman -Syu</code>

<h3 id="6">6. Set Waktu dan Bahasa</h3>
<h4 id="6a">a. Set waktu</h4>
Untuk set waktu tentu saja mimin pilih yang sesuai tanah kelahiran
<code>ln -sf /usr/share/zoneinfo/Asia/Makassar /etc/localtime
hwclock --systohc</code>

<h4 id="6b">b. Set bahasa</h4>
Silahkan pilih bahasa yang Anda inginkan. Kalau mimin terbiasa pakai bahasa Inggris jadi uncomment "en_US.UTF-8"
<code>nano /etc/locale.gen</code>

Generate your desired locales running:
<code>locale-gen
export LANG="en_US.UTF-8"
export LC_COLLATE="C"</code>

<h3 id="7">7. Instal Bootloader</h3>
Untuk bootoader kita pake yang umum aja, grub. Biasanya grub akan mendeteksi file image hasil generate dari perintah "mkinitcpio -p linux" yang sebelumnya dari proses instalasi kernel. Os-prober gunanya untuk mendeteksi OS lain jika Anda ingin dual booting. Efibootmgr untuk instalasi bootloader pada partisi efi yang kita udah siapkan sebelumnya. Dalam hal ini /dev/sda1 yang kita mount di folder /mnt/boot
<code>pacman -S grub os-prober efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub</code>

Selanjutnya generate file grub.cfg biar bisa tampil saat bootup
<code>grub-mkconfig -o /boot/grub/grub.cfg</code>

<h3 id="8">8. Tambah user/pengguna</h3>
Kita set password untuk root dulu
<code>passwd</code>

Terus, kita bikin user yang akan Anda gunakan sehari-hari, misal nama usernya adalah jack.
<code>useradd -m -G wheel jack</code>

Kasih password buat username jack
<code>passwd jack</code>

Terus edit sudoers, biar si jack bisa dapat hak eksekusi perintah sebagai bagian dari grup "wheel". Hilangkan tanda pagar pada bagian %wheel ALL=(ALL) ALL.
<code>nano /etc/sudoers
uncomment -->   %wheel ALL=(ALL) ALL</code>

<h3 id="9">9. Pengaturan Jaringan</h3>
<h4 id="9a">a. Create hostname files</h4>
Nama hostnamenya terserah Anda, kalau mimin kasih nama Lontara
<code>nano /etc/hostname
Lontara</code>
Ctrl+X save and exit

Now add matching entries to hosts:
<code>nano /etc/hosts
 127.0.0.1        localhost
 ::1              localhost
 127.0.1.1        Lontara.localdomain  Lontara</code>

Next
<code>nano /etc/conf.d/hostname 
hostname="Lontara"</code>

<h4 id="9b">b. Install connman</h4>
<code>pacman -S wpa_supplicant connman connman-openrc connman-gtk</code>

<h4 id="9c">c. Konfigurasi connman</h4>
Temukan bagian "AllowHostnameUpdates = true" ubah ke "false". Konfigurasi ini berguna agar connman tidak mengubah isi file hostname.
<code>nano /etc/connman/main.conf</code>

Buat agar connman bisa jalan otomatis saat boot up.
<code>rc-update add connmand default</code>

<h3 id="10">10. Reboot The System</h3>
Sekarang Anda bisa exit dari chroot, unmount semua partisi lalu reboot.
<code>exit
sudo umount -R /mnt
sudo reboot</code>

Di sini Anda sudah bisa <a href="https://olderdeserved.com/pt9s4dh3g?key=fdd5b94e99220fe83e176ef790d9d5cc" rel="nofollow" target="_blank">eject live CD Anda.</a> Jadi setelah Anda reboot dan melihat grub menu, artinya proses instalasi Anda berhasil. Pastikan sebelum booting, UEFI Menu Anda set ke partisi yang memiliki file system EFI di dalamnya. Silahkan pilih boot menu paling atas, lalu biarkan sistem Anda loading, kemudian masuk ke sistem sebagai user yang Anda buat. Misalnya pada tutorial sebelumnya kita buat username jack lalu masukkan passwordnya.

<h3 id="11">11. Instal Yaourt</h3>
Banyak yang bilang yaourt udah obsolete, karena udah bertahun-tahun gak update. So what?! Bagi mimin, meskipun obsolete tapi kalau masih berguna yah tetap mimin pakai! Mimin orangnya setia kok :p. Jadi yaourt ini semacam alternatif dari pacman, tugasnya sama, yaitu mengurus soal paket dan instalasi. Bedanya karena yaourt bisa akses ke aur, pacman gak bisa.
<code>sudo pacman -S yaourt</code>

<h3 id="12">12. Instal Drivers</h3>
<h4 id="12a">a. Driver grafis</h4>
Silahkan pilih satu aja, atau dua jika Anda punya teknologi dual graphic cards, atau bahasa kerennya Optimus.
<code>sudo pacman -S xf86-video-intel   <- untuk grafis intel
sudo pacman -S nvidia   <- untuk grafis nvidia
sudo pacman -S nouveau   <- open source nvidia
sudo pacman -S xf86-video-amdgpu
sudo pacman -S xf86-video-ati
sudo pacman -S xf86-video-vmware   <- untuk vmware atau virtualbox</code>

Harap diperhatikan, nvidia sekarang udah gak support kartu grafis tua, macam punya mimin nvidia geforce 920M. Jadi mimin biasanya instal
<code>yaourt -S nvidia-390xx nvidia-390xx-dkms</code>
Jadi silahkan <a href="https://nessainy.net/4/4938810" rel="nofollow" target="_blank">baca-baca Arch Wiki</a> untuk bisa tau kartumu support atau gak.

Bagian ini opsional untuk yang punya teknologi hybrid atau optimus, mimin saranin pakai bumblebee.
<code>yaourt -S bumblebee bumblebee-openrc primus bbswitch</code>

Lalu edit konfigurasi bumblebee
<code>sudo nano /etc/bumblebee/bumblebee.conf</code>
Temukan baris ini
<code>Driver=nvidia
Bridge=primus
VGLTransport=rgb
PMMethod=bbswitch</code>

Lalu masukkan username Anda dalam grup bumblebee lalu pastikan bumblebee aktif saat boot up.
<code>gpasswd -a <username> bumblebee
sudo rc-update add bumblebee default</code>

<h4 id="12b">b. Driver audio</h4>
<code>sudo pacman -S pulseaudio-alsa</code>

<h3 id="13">13. Instal Display Manager</h3>
Display Manager ini kalau <a href="https://crackjack.info/post/phoenix-liteos-10-pro-gamer-21h2-build-190441415-x64" alt="Windows 10 iso">diWindows namanya Login Screen.</a> Dan untuk linux tentu banyak sekali pilihan tema. Mimin saranin pake lightdm aja.
<code>sudo pacman -S lightdm lightdm-gtk-greeter lightdm-openrc
sudo rc-update add lightdm sysinit</code>

<h3 id="14">14. Instal Compiz Desktop</h3>
Compile compiz dari aur dan emerald sebagai window managernya.

<h4 id="14a">a. Desktop Environment</h4>
Compiz terbaru versi 0.9, itu udah include panel konfigurasi dan semua pluginnya.
<code>yaourt -S compiz</code>

Tapi kalau semisal compiz 0.9 gagal compile, yah kita turunin versinya ke 0.8. Versi ini diinstal terpisah, mulai dari core, konfigurasi, sampai pluginsnya.
<code>yaourt -S ccsm-gtk3 compiz-fusion-plugins-main compiz-fusion-plugins-extra</code>

Instal window manager
<code>yaourt -S emerald</code>

<h4 id="14b">b. Create file compiz-session</h4>
Pada dasarnya compiz gak bisa berdiri sendiri, jadi kita bikinin file session sendiri.
<code>sudo nano /usr/bin/compiz-session</code>

Isi dengan
<code>#!/bin/bash
if test -z "$DBUS_SESSION_BUS_ADDRESS";
then
eval 'dbus-launch --sh-syntax --exit-with-session'
fi
compiz --replace ccp & wmpid=$!
sleep 1
if [ -f ~/.compiz-session ]; then
source ~/.compiz-session &
else
xterm &
fi
wait $wmpid</code>

Bikin filenya bisa dieksekusi
<code>sudo chmod 755 /usr/bin/compiz-session</code>

<h4 id="14c">c. Create a desktop file for compiz session</h4>
Cek dulu folder xsessions, kalau semisal belum ada, buat dulu.
<code>sudo mkdir /usr/share/xsessions</code>

Lalu bikin file desktopnya
<code>sudo nano /usr/share/xsessions/compiz-session.desktop</code>

Isi dengan
<code>[Desktop Entry]
Name=Compiz
Comment=Compiz standalone
Exec=compiz-session
Type=Application</code>

<h4 id="14d">d. Create a .compiz-session file</h4>
Jadi file .compiz-session diletakkan di folder home Anda, isinya berupa program yang akan dieksekusi saat Anda login ke desktop.
<code>nano .compiz-session</code>

Isi dengan
<code>#!/bin-bash
xfce4-panel &
xfce4-power-manager &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
connman-gtk &
randomwall.sh &</code>

Instal xdg-user-dirs untuk generate isi dari partisi home Anda, lebih tepatnya di folder home user Anda.
<code>sudo pacman -S xdg-user-dirs
xdg-user-dirs-update</code>

<h3 id="15">15. Fungsi Dasar</h3>
Dibagian ini kita udah bisa instal berbagai program dasar untuk fungsi dasar.</br>
a. File System mounter
<code>sudo pacman -S gvfs gvfs-mtp ntfs-3g</code>

b. File Manager
<code>yaourt -S thunar tumbler ffmpegthumbnailer thunar-volman thunar-archive-plugin xfce4-settings xarchiver-gtk2 zip unzip unrar mousepad lxtask</code>

c. Sound
<code>sudo pacman -S pavucontrol paprefs</code>

d. Terminal dan Panel
<code>yaourt -S xfce4-panel xfce4-notifyd xfce4-clipman-plugin xfce4-terminal xfce4-power-manager xfce4-mount-plugin gmrun oblogout-py3-git</code>

e. Personalization
<code>yaourt -S lxappearance-gtk3 vertex-themes reversal-icon-theme-git</code>

<h3 id="16">16. Install Goodies</h3>
a. Primer
<code>yaourt -S baobab localepurge bleachbit ungoogled-chromium</code>

b. Sekunder
<code>yaourt -S mpv viewnior neofetch gnome-font-viewer</code>

<h3 id="17">17. Misc</h3>
<h4 id="17a">a. Customize oblogout</h4>
Disini Anda juga bisa mengubah icon apa saja yang ditampilkan oblogout, dan perintah apa saja yang dieksekusi.
<code>sudo nano /etc/oblogout.conf</code>

Cari baris ini
<code>shutdown    = sudo poweroff
restart     = sudo reboot
logout = killall compiz-session</code>

Edit sudoers
<code>sudo mousepad /etc/sudoers</code>
Tambahkan ini
<code>%wheel ALL=NOPASSWD: /usr/bin/poweroff,/usr/bin/reboot</code>
Dengan ini, semua user di dalam wheel group tidak perlu password untuk reboot atau poweroff

<h4 id="17b">b. Simple wallpaper changer with feh</h4>
Randomly change wallpapers in 5 minutes.
Instal feh dulu
<code>sudo pacman -S feh</code>

Letakkan semua wallpaper Anda di /usr/share/backgrounds/ lalu bikin file
<code>sudo nano /usr/bin/randomwall.sh</code>

Link to <a href="https://www.mediafire.com/file/3d48n6xe6uy0xy8/Jack.zip/file" rel="nofollow" target="_blank">download the wallpapers.</a>

Isi dengan ini
<code>#!/bin/bash
while true
WALLPAPERS="/usr/share/backgrounds"
do
feh -z --bg-fill $WALLPAPERS
sleep 300
done</code>

Bikin filenya bisa dieksekusi
<code>sudo chmod +x /usr/bin/randomwall.sh</code>

Lalu letakkan difile .compiz-session di folder home Anda agar script ini bisa jalan saat login.

<h4 id="17c">c. Configure Localepurge</h4>
Localepurge is an app to delete all unused languages and man files
<code>sudo mousepad /etc/locale.nopurge</code>
comment this line
<code>NEEDSCONFIGFIRST</code>

Uncomment these
<code>MANDELETE
DONTBOTHERNEWLOCALE</code>

Exclude your langs from deletion
<code>en
en_US
en_US.UTF-8</code>

Sekarang, setelah Anda menginstal sesuatu, eksekusi sudo localepurge diterminal. Yes, welldone sir! Dari sini Anda sudah reboot lalu login ke desktop Anda.

<img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEh4RnyC2DenlmiwXz2b7hq2Zy25_jZ5W9RX3y9b9y9GkwuKKgjCwrO1_pDqMyJ1AwumWaETSXMoMsoeCTR3Iz6zIXoNYwhuhhwh3Y0MUhSR_zK0uRnl5VSIBlVkIEAOlm7KpvQVL8KydlMpNHaVjXhXEdlvAPqCN93NJBoVDbaZ3nvpztKifLXj9pbk2g/s16000/artixcompiz1.jpg" alt="Cara Instal Compiz Desktop di Artix Linux" />
